import React from 'react';
import {createRoot} from 'react-dom/client';
import './index.styles.css';
import AppView from './App.view';

const container = document.getElementById('root')!;
const root = createRoot(container);
root.render(<AppView/>);
