import React from "react";

import TranslationBoxes from "./components/TranslationBoxes/TranslationBoxes.component";
import JokeGenerator from "./components/JokeGenerator/JokeGenerator.component";

import "./App.styles.css";
const pirateLogo = require("./assets/images/digital_pirate_pixel_2.png");

function AppView() {
  return (
    <div id="App">
      <div id="AppContainer">
        <div id="Header">
          <img src={pirateLogo} alt="digital pirate logo"/>
          <label>Pirate Tongue.HTML</label>
        </div>
        <TranslationBoxes />
        <JokeGenerator />
      </div>
    </div>
  );
}

export default AppView;
