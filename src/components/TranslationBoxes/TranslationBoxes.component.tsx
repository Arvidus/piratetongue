import React, { ChangeEvent, Dispatch, useState } from "react";
import { translate } from "../../functions/translate.function";

import "./TranslationBoxes.styles.css";

const TranslationBoxes = () => {
  const [victimText, setVictimText] = useState("");
  const [pirateText, setPirateText] = useState("");

  const handleTranslation = (
    e: ChangeEvent<HTMLTextAreaElement>,
    setText: Dispatch<string>,
    setTranslation: Dispatch<string>,
    youAreAPirate: boolean
  ) => {
    setText(e.target.value);
    setTranslation(translate(e.target.value, youAreAPirate, false));
  };

  return (
    <div id="BoxContainer">
      <div className="box">
        <label>Victim</label>
        <textarea
          value={victimText}
          onChange={(e) =>
            handleTranslation(e, setVictimText, setPirateText, true)
          }
        />
      </div>
      <div className="box">
        <label>Pirate</label>
        <textarea
          value={pirateText}
          onChange={(e) =>
            handleTranslation(e, setPirateText, setVictimText, false)
          }
        />
      </div>
    </div>
  );
};

export default TranslationBoxes;
