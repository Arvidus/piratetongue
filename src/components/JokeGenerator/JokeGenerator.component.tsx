import React, { useState } from "react";
import jokeApi from "../../api/joke.api";
import { translate } from "../../functions/translate.function";

import "./JokeGenerator.styles.css";

const JokeGenerator = () => {
  const [joke, setJoke] = useState({ joke: "", setup: "", delivery: "" });
  const [translated, setTranslated] = useState(true);

  const getJoke = async () => {
    await jokeApi
      .get(
        "/joke/Any?blacklistFlags=nsfw,religious,political,racist,sexist,explicit"
      )
      .then(async (res) => {
        if (res.data.setup)
          await setJoke({
            joke: "",
            setup: translate(res.data.setup, true, false),
            delivery: translate(res.data.delivery, true, false),
          });
        else
          await setJoke({
            joke: translate(res.data.joke, true, false),
            setup: "",
            delivery: "",
          });
      });
  };

  const handleTranslation = async () => {
    setJoke({
      joke: await translate(joke.joke, false, true),
      setup: await translate(joke.setup, false, true),
      delivery: await translate(joke.delivery, false, true),
    });
    setTranslated(true);
  };

  return (
    <div id="JokeBox">
      <button
        onClick={async () => {
          await getJoke();
          setTranslated(false);
        }}
      >
        Tell me a joke
      </button>
      {joke.joke && <p>{joke.joke}</p>}
      {joke.setup && <p>{joke.setup}</p>}
      {joke.delivery && <p>{joke.delivery}</p>}
      {!translated && (
        <button onClick={handleTranslation}>I'm no pirate!</button>
      )}
    </div>
  );
};

export default JokeGenerator;
