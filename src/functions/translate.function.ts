import { consonants } from "../data/consonants.data";

export const translate = (
  input: string,
  youAreAPirate: boolean,
  runARig: boolean
) => {
  let translation = "";
  let tempPhrase = "";

  if (youAreAPirate) {
    input.split("").forEach((character) => {
      tempPhrase = character;
      consonants.forEach((consonant) => {
        switch (character) {
          case "X":
            if (!runARig) tempPhrase = "Koksos";
            break;
          case "x":
            if (!runARig) tempPhrase = "koksos";
            break;
          case consonant:
            tempPhrase = consonant + "o" + consonant.toLowerCase();
            break;
        }
      });
      translation = translation + tempPhrase;
    });
  } else {
    tempPhrase = input;
    if (!runARig)
      if (input.toLowerCase().includes("koksos")) {
        let replaceX = new RegExp("koksos", "g");
        for (let i = 0; i < (input.match(replaceX) || []).length; i++) {
          tempPhrase = tempPhrase.toLowerCase().replace("koksos", "x");
        }
      }
    consonants.forEach((consonant) => {
      let replaceC = new RegExp(consonant + "o" + consonant.toLowerCase(), "g");
      if ((input.match(replaceC) || []).length > 0) {
        for (let i = 0; i < (input.match(replaceC) || []).length; i++) {
          tempPhrase = tempPhrase.replace(
            consonant + "o" + consonant.toLowerCase(),
            consonant
          );
        }
        translation = tempPhrase;
      }
    });
  }
  return translation;
};
